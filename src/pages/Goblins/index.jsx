import React from 'react';
import {Route, Switch} from 'react-router-dom';
import List from "./List";

const Goblins = ({match: {path}}) => (
    <Switch>
        <Route path = {`${path}/`} strict component = {List} />
    </Switch>
);

export default Goblins;