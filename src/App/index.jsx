import React, {Fragment} from 'react';
import Header from './Header';
import Footer from './Footer';
import {
    BrowserRouter as Router, 
    Switch, 
    Route
} from 'react-router-dom'
import {Container, Row, Col} from 'reactstrap';
import Home from './home';
import Goblins from '../pages/Goblins';
import Fairys from '../pages/Fairys';
import Sorcerers from '../pages/Sorcerers';

const Index = () => ( 
  <Fragment>
        <Router>
            <Container>
                <Row>
                    <Col>
                            <Header/>
                                <main>
                                    <Switch>
                                        <Route path = "/sorcerers" component = {Sorcerers} />
                                        <Route path = "/fairys" component = {Fairys}/>
                                        <Route path = "/goblins" component = {Goblins}/>
                                        <Route path = "/" component = {Home}/>
                                    </Switch>
                                </main>
                            <Footer/>
                    </Col>
                </Row>
            </Container>  
        </Router>
  </Fragment>
)

export default Index;