import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';
import Logo from '../components/images/Logo.png'
import IG from '../components/images/ig.png';
import FB from '../components/images/fb.png';
import { Link } from 'react-router-dom';

const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
      <header>
            <Navbar className=" fixed-top navbar-expand"color="success" light expand="md">
            <NavbarBrand tag={Link} to="/"> <img src={Logo}width={45} height={45} alt="35x35" /> Artesanías</NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
            <Nav className="navbar-nav mr-auto ml-auto text-center-auto" navbar>
                <NavItem>
                <NavLink tag={Link} to="/goblins">Duendes</NavLink>
                </NavItem>
                <NavItem>
                <NavLink tag={Link} to="/fairys">Hadas</NavLink>
                </NavItem>
                <NavItem>
                <NavLink tag={Link} to="/sorcerers">Hechiceros</NavLink>
                </NavItem>
            </Nav>
            </Collapse>
            <div className="d-flex flex-row d-flex justify-content-center">
                    <NavbarBrand href="https://www.instagram.com/artesanias_mariam/"> <img src={IG}width={45} height={45} alt="35x35"/></NavbarBrand>
                    <NavbarBrand  href="https://www.facebook.com/mariamartesanias2014"> <img src={FB} height= {40} alt="50x50"/></NavbarBrand>
                </div>
            </Navbar>
      </header>
  );
}

export default Header;