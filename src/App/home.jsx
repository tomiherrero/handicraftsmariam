import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';
import Goblins from '../components/images/Goblins.jpeg';
import Florys from '../components/images/Florys.jpeg';

const items = [{
    src: Goblins,
    height: 750,
    width: 400,
    key: '1'
  },
  {
      src: Florys,
      key: '2'
  }
];
const Home = () => <UncontrolledCarousel items={items} />;

export default Home;
